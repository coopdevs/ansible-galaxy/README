[[_TOC_]]

# Publishing collections to Ansible Galaxy from Gitlab
Although the main integration of Ansible Galaxy is with Github, it is also possible to push collections from Gitlab, using CI/CD pipelines.

## Developing a Ansible collection
Ansible Galaxy comes with cli commands for building and publishing collections.

To scaffold a collection, run the next command:
```bash
ansible-galaxy collection init namespace.collection
```
where *namespace* should be `coopdevs`.

> Note that Ansible collections must match `a[a-z0-9_]` (no hyphens allowed).

It will create a folder structure like this:
```
.
├── docs
├── galaxy.yml
├── plugins
│   └── README.md
├── README.md
└── roles
```

- First, we need edit `galaxy.yml` file and fill in as many  fields  we can. (There's [an example](https://gitlab.com/coopdevs/ansible-galaxy/ubuntu_netplan_dns/-/blob/main/galaxy.yml) for a working `galaxy.yml`)

- Then, we have to create `meta/runtime.yml` and fill it like this:
```yaml
---
requires_ansible: '>=2.9.10'
```

- Afterwards, we checkout the roles directory (`cd roles`) and initialize them running this command as times as roles we want in the collection :
 ```bash
ansible-galaxy role init rol_name
```
  This will create a role folder estructure. We fill `meta/main.yml`, `README` and develop the role itself.

In that point, we should be able to publish the collection.

## Publishing from Gitlab CI
First, we must create and publish the repository, within the `ansible-galaxy` subgroup in the `coopdevs` group. The name must match the name chosen for the collection in the previous steps (remember: no hyphens).

### Credentials
In order to be able to publish from Gitlab, we must have the [Ansible Galaxy API key](https://galaxy.ansible.com/me/preferences) stored in our Gitlab CI/CD variables. If you repository is in the sub-group `coopdevs/ansible-galaxy`, it is already set.

### Pipeline

The pipeline configured at `.gitlab-ci.yml` must have, at least, the following two jobs:

```yaml
stages:
- build
- publish

image: enmanuelmoreira/docker-ansible-alpine:latest
  
build:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - ansible-galaxy collection build --force
  artifacts:
    paths: - $CI_BUILDS_DIR/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.gz
	expire_in: 30 days

publish:
  stage: publish
  rules:
    - if: $CI_COMMIT_TAG
  dependencies: 
    - build
  script:
    - ansible-galaxy collection publish $CI_BUILDS_DIR/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.gz --token $GALAXY_TOKEN
```

### Building
The pipeline is configure to run build & release jobs only if there is a tag linked to the last commit. 

So, after updating the repository, we can activate the pipeline by creating a tag.

> **The tag name must match the `version` specified in `galaxy.yml`**.
> This is because the `build` job creates a tarball with the reported galaxy tag, while the `publish` job uses the commit tag to fetch the tarball and upload it to Ansible Galaxy.

If the pipeline finishes without errors, the collection will already be published to Ansible Galaxy.

If any errors were thrown, they will show up in both the GitLab Pipeline and Ansible Galaxy notifications.


# TODO
Further research is needed to be able to push only a role -without belonging to any collection. First exploration (`travis.yml` and [documentation](https://stackoverflow.com/questions/61715681/publish-ansible-role-to-ansible-galaxy-with-gitlab-ci)) tells that it must be created at Github. 

There's no `ansible-galaxy role publish` command.
